#-------------------------------------------------
#
# Project created by QtCreator 2016-09-28T13:41:26
#
#-------------------------------------------------

#QT       -= gui
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VotumTouchEmulator
TEMPLATE = lib

DEFINES += VOTUMTOUCHEMULATOR_LIBRARY

SOURCES += votumtouchemulator.cpp \
        pointscontroller.cpp \
    mousecompatibility.cpp \
    acceptsmodifiedmouseevents.cpp

HEADERS += votumtouchemulator.h\
        votumtouchemulator_global.h \
        pointscontroller.h \
    mousecompatibility.h \
    acceptsmodifiedmouseevents.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -LC:/Users/user/libs/hidapi/windows/Release/ -lhidapi
else:win32:CONFIG(debug, debug|release): LIBS += -LC:/Users/user/libs/hidapi/windows/Debug/ -lhidapi

INCLUDEPATH += C:/Users/user/libs/hidapi/hidapi
DEPENDPATH += C:/Users/user/libs/hidapi/hidapi
