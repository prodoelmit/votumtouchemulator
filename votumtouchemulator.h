#ifndef VOTUMTOUCHEMULATOR_H
#define VOTUMTOUCHEMULATOR_H
#include <QTouchEvent>
#include <QScreen>
#include <QTimer>
#include "hidapi.h"
#include "pointscontroller.h"


#include "votumtouchemulator_global.h"

class  VotumTouchEmulator: public QObject
{
    Q_OBJECT

public:
    VotumTouchEmulator(int vid = 0x2575, int pid = 0x000a);

    QPointF mapFromVotum(const QPointF& p);

public slots:
    void pokeHID();
    void requestFeature();

private:

    quint64 m_lastRequestTime;

    hid_device* m_device;
    PointsController* m_pC;
};

#endif // VOTUMTOUCHEMULATOR_H
