#ifndef MOUSECOMPATIBILITY_H
#define MOUSECOMPATIBILITY_H
#include <QObject>
#include "votumtouchemulator_global.h"


class MouseCompatibility: public QObject
{
    Q_OBJECT
public:
    MouseCompatibility();
protected:
    bool eventFilter(QObject* obj, QEvent *event);

};

#define INSTALLMC MouseCompatibility* mcompatibility = new MouseCompatibility; installEventFilter(mcompatibility);
#endif // MOUSECOMPATIBILITY_H
