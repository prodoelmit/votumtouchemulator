#include "pointscontroller.h"
#include <QAbstractScrollArea>
#include <QTouchEvent>
#include "qdebug.h"
#include <QMouseEvent>
#include <QApplication>
#include <QWidget>

PointsController::PointsController()
    : curWidget(0)
{
    for (int i = 0; i < 5 ; i++) {
        m_states << NothingHappens;
        QTouchEvent::TouchPoint tp(0);
        tp.setState(Qt::TouchPointStationary);
        tp.setId(i);
        m_points << tp;
    }

}

void PointsController::update(int stylusId, int state, QPointF point)
{
    if (stylusId >= 5) {
        qWarning("stylusId is >= 5");
        return;
    }
    QTouchEvent::TouchPoint& p = m_points[stylusId];
    State prevState = m_states[stylusId];
    State newState;
//    qDebug() << "PID = " << stylusId;
//    qDebug() << "prevState = " << prevState;
//    qDebug() << "eventState = " << state;
    State eventState = (State)(state);
    switch (prevState) {
    case NothingHappens:
        switch (eventState) {
        case TouchUpdate:
            newState = TouchBegin;
            break;
        case NothingHappens:
            newState = NothingHappens;
            break;
        }
        break;

    case TouchBegin:
        switch (eventState) {
        case TouchUpdate:
            newState = TouchUpdate;
            break;
        case NothingHappens:
            newState = TouchEnd;
            break;
        }
        break;

    case TouchUpdate:
        switch (eventState) {
        case TouchUpdate:
            newState = TouchUpdate;
            break;
        case NothingHappens:
            newState = TouchEnd;
            break;
        }
        break;

    case TouchEnd:
        switch (eventState) {
        case TouchUpdate:
            newState = TouchBegin;
            break;
        case NothingHappens:
            newState = NothingHappens;
            break;
        }
        break;
    }

//    qDebug() << "newState = " << newState;

    switch (newState) {
    case TouchBegin:
        p.setStartScreenPos(point);
        p.setScreenPos(point);
        p.setState(Qt::TouchPointPressed);
        break;
    case TouchUpdate:
        p.setScreenPos(point);
        p.setState(Qt::TouchPointMoved);
        break;
    case TouchEnd:
        p.setScreenPos(point);
        p.setLastScreenPos(p.screenPos());
        p.setState(Qt::TouchPointReleased);
        break;
    case NothingHappens:
        break;
    }


    m_states[stylusId] = newState;


    for (int i = 0; i < 5; i++) {
        if (i == stylusId) continue;

        State& s = m_states[i];
        if (s == TouchBegin) {
            s = TouchUpdate;
        } else if (s == TouchEnd) {
            s = NothingHappens;
        }
    }

    emitEvent();
}

void PointsController::emitEvent()
{
    QTextStream out(stdout);
    bool hasBegin = m_states.contains(TouchBegin);
    bool hasUpdate = m_states.contains(TouchUpdate);
    bool hasEnd = m_states.contains(TouchEnd);
    bool hasNothing = m_states.contains(NothingHappens);

    QTouchEvent::Type type;
    if (hasBegin) {
        if (hasUpdate || hasEnd) {
            type = QTouchEvent::TouchUpdate;
        } else {
            type = QTouchEvent::TouchBegin;
        }
    } else if (hasEnd) {
        type = QTouchEvent::TouchEnd;
    } else if (hasUpdate) {
        type = QTouchEvent::TouchUpdate;
    }  else {
        type = QTouchEvent::None;
    }


//    qDebug() << QTouchEvent::None << " " << type;
    if (type != QTouchEvent::None) {

        Qt::TouchPointStates state = 0;
        for (int i = 0; i < 5; i++) {
            state ^= m_points[i].state();
        }

        QList<QTouchEvent::TouchPoint> ps;
        for (int i = 0; i < 5; i++) {
            if (m_states[i] != NothingHappens) {
                ps << m_points[i];
//                qDebug() << m_points[i].id();
            }

        }
//        qDebug() << m_points.first().screenPos();

        QTouchEvent* e = new QTouchEvent(type, QTouchEvent::TouchScreen, Qt::NoModifier, state, ps);
        if (e->type() == QTouchEvent::TouchBegin) {
            QPoint p = ps.first().screenPos().toPoint();
            QWidget* w = QApplication::widgetAt(p);

            if (w) {
                curWidget = w;
                QCoreApplication::postEvent(w, e);
                qDebug() << "Posted event type " << e->type() << " on widget " << w;
            } else {
                w = 0;
            }
        } else {
//            QWidget* w = QApplication::focusWidget();
            QWidget* w = curWidget;
            QAbstractScrollArea* a = dynamic_cast<QAbstractScrollArea*>(w);
            if (a) {
                w = a->viewport();
            }
            QCoreApplication::postEvent(w, e);
                qDebug() << "Posted event type " << e->type() << " on widget " << w;
        }
    }


}
