#include "votumtouchemulator.h"
#include <QDateTime>
#include <QApplication>
#include <QDesktopWidget>
#include "qdebug.h"

#include <Windows.h>

VotumTouchEmulator::VotumTouchEmulator(int vid, int pid)
    : m_lastRequestTime(0)
{

    QTextStream out(stdout);
    qDebug() << "hey you";
    m_pC = new PointsController;
    m_device = hid_open(vid, pid, NULL);
    if (m_device) {
            hid_set_nonblocking(m_device, 1);

            QTimer::singleShot(0, this, SLOT(pokeHID()));
    }
}

QPointF VotumTouchEmulator::mapFromVotum(const QPointF &p)
{

    QDesktopWidget* widget = QApplication::desktop();
    QRect size = widget->screenGeometry(widget->primaryScreen());
//    QSize size = QApplication::focusWindow()->screen()->size();

    qreal w = size.width();
    qreal h = size.height();
    qreal x = p.x() / 1024. * w;
    qreal y = p.y() / 768. * h;

//    qDebug() << x << y;
    return QPointF(x,y);

}

void VotumTouchEmulator::pokeHID()
{
    requestFeature();
    unsigned char data[29];
    int success = hid_read(m_device, data, 29);

    if (success) {
        QVector<QPointF> points;
        bool notNull = false;

        for (int i = 0; i < 5; i++) {

            qreal x = 1023 * (data[ 1+i*5+1] + 256*data[ 1+i*5+2] ) /32767.;
            qreal y = 767 * (data[ 1+i*5+3] + 256*data[ 1+i*5+4] ) /32767.;


            QPointF p(x,y);
            points << mapFromVotum(p);
            notNull = notNull || (p.manhattanLength() > 0);

        }

        int stylusId = data[26];
//        qDebug() << stylusId;
        int statusCode = data[1];

//        qDebug() << points.first();
        m_pC->update(stylusId, statusCode, points.first());

    }


    QTimer::singleShot(0, this, SLOT(pokeHID()));

}

void VotumTouchEmulator::requestFeature()
{
    QTextStream out(stdout);
    int threshold = 10;
    if (QDateTime::currentMSecsSinceEpoch() - m_lastRequestTime < threshold) return;
    if (QDateTime::currentMSecsSinceEpoch() - m_lastRequestTime > 100) {qWarning("WARNING!100YEARSPASSED!");}

    unsigned char send_data[2];
    send_data[0] = (unsigned char)(2);
    send_data[1] = (unsigned char)(0x6F);

    int send_status = hid_send_feature_report(m_device, send_data, 2);
    m_lastRequestTime = QDateTime::currentMSecsSinceEpoch();
//    out << m_lastRequestTime << " " << send_status << endl;
    Sleep(5);

}
