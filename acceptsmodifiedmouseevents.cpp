#include "acceptsmodifiedmouseevents.h"

AcceptsModifiedMouseEvents::AcceptsModifiedMouseEvents()
{

}

int AcceptsModifiedMouseEvents::getStylusIdFromModifiedType(QEvent::Type type)
{
    return type >> 10;
}

QEvent::Type AcceptsModifiedMouseEvents::getTypeFromType(QEvent::Type type)
{
    return QEvent::Type(255 & type);
}

