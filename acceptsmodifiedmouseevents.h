#ifndef ACCEPTSMODIFIEDMOUSEEVENTS_H
#define ACCEPTSMODIFIEDMOUSEEVENTS_H
#include <QEvent>
#include "votumtouchemulator_global.h"


class VOTUMTOUCHEMULATORSHARED_EXPORT AcceptsModifiedMouseEvents
{
public:
    AcceptsModifiedMouseEvents();

    int getStylusIdFromModifiedType(QEvent::Type type);
    QEvent::Type getTypeFromType(QEvent::Type type);
};

#endif // ACCEPTSMODIFIEDMOUSEEVENTS_H
