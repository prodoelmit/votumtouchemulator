#ifndef POINTSCONTROLLER_H
#define POINTSCONTROLLER_H
#include <QTouchEvent>

#include <QVector>

class PointsController
{
public:
    PointsController();
    void update(int stylusId, int state, QPointF point);
    enum State {
        TouchBegin = 6,
        TouchUpdate = 8,
        TouchEnd = 2,
        NothingHappens = 0
    };


    void emitEvent();


private:
    QVector<State> m_states;
    QList<QTouchEvent::TouchPoint> m_points;
    QWidget* curWidget;


};

#endif // POINTSCONTROLLER_H
