#include "mousecompatibility.h"
#include "acceptsmodifiedmouseevents.h"
#include <QWidget>
#include <QEvent>
#include "qdebug.h"
#include <QTouchEvent>
#include <QMouseEvent>


MouseCompatibility::MouseCompatibility()
{

}

bool MouseCompatibility::eventFilter(QObject *obj, QEvent *event)
{
    switch (event->type()) {
    case QEvent::TouchBegin: {
        QWidget* w = dynamic_cast<QWidget*>(obj);
        QTouchEvent* e = dynamic_cast<QTouchEvent*>(event);
        QEvent::Type type = QMouseEvent::MouseButtonPress;
        if (dynamic_cast<AcceptsModifiedMouseEvents*>(obj)) {
            int id = e->touchPoints().first().id();
            int code = id << 10;
            type = (QEvent::Type)(type | code);
        }
        QMouseEvent* ev = new QMouseEvent(
                    type
                    , w->mapFromGlobal(e->touchPoints().first().screenPos().toPoint())
                    , e->touchPoints().first().screenPos().toPoint()
                    , Qt::LeftButton
                    , Qt::LeftButton
                    , Qt::NoModifier);
        qDebug() << "Created mousePress event";
        return QObject::eventFilter(obj, ev);
        break;
    }
    case QEvent::TouchUpdate: {
        QWidget* w = dynamic_cast<QWidget*>(obj);
        QTouchEvent* e = dynamic_cast<QTouchEvent*>(event);
        QEvent::Type type = QMouseEvent::MouseMove;
        if (dynamic_cast<AcceptsModifiedMouseEvents*>(obj)) {
            int id = e->touchPoints().first().id();
            int code = id << 10;
            type = (QEvent::Type)(type | code);
        }
        QMouseEvent* ev = new QMouseEvent(
                    type
                    , w->mapFromGlobal(e->touchPoints().first().screenPos().toPoint())
                    , e->touchPoints().first().screenPos().toPoint()
                    , Qt::NoButton
                    , Qt::LeftButton
                    , Qt::NoModifier
                    );
        qDebug() << "Created mouseMove event";
        return QObject::eventFilter(obj, ev);
        break;
    }
    case QEvent::TouchEnd: {
        QWidget* w = dynamic_cast<QWidget*>(obj);
        QTouchEvent* e = dynamic_cast<QTouchEvent*>(event);
        QEvent::Type type = QMouseEvent::MouseButtonRelease;
        if (dynamic_cast<AcceptsModifiedMouseEvents*>(obj)) {
            int id = e->touchPoints().first().id();
            int code = id << 10;
            type = (QEvent::Type)(type | code);
        }
        QMouseEvent* ev = new QMouseEvent(
                    type
                    , w->mapFromGlobal(e->touchPoints().first().screenPos().toPoint())
                    , e->touchPoints().first().screenPos().toPoint()
                    , Qt::LeftButton
                    , Qt::NoButton
                    , Qt::NoModifier
                    );
        return QObject::eventFilter(obj, ev);
        break;
    }
    default:
        return QObject::eventFilter(obj, event);
        break;
    }



//    if (event->type() == QEvent::TouchBegin) {
//        qDebug() << "MouseCompatibility: TouchBegin registered";
//        return true;
//    } else if (event->type() == QEvent::MouseButtonPress) {
//        qDebug() << "MouseCompatibility: MouseButtonPress registered";
//        return QObject::eventFilter(obj, event);
//    } else {
//        return QObject::eventFilter(obj, event);

//    }
}
