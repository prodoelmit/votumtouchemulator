SOURCES += votumtouchemulator.cpp \
        pointscontroller.cpp \
    mousecompatibility.cpp \
    acceptsmodifiedmouseevents.cpp

HEADERS += votumtouchemulator.h\
        votumtouchemulator_global.h \
        pointscontroller.h \
    mousecompatibility.h \
    acceptsmodifiedmouseevents.h


#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/libs/hidapi/windows/Release/ -lhidapi
#else:win32:CONFIG(debug, debug|release): LIBS += -LC:/Users/user/libs/hidapi/windows/Debug/ -lhidapi

#INCLUDEPATH += C:/Users/user/libs/hidapi/hidapi
#DEPENDPATH += C:/Users/user/libs/hidapi/hidapi
